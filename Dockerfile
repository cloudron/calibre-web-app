FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/pkg /app/code
WORKDIR /app/code

RUN apt update && \
    apt install -y python3-setuptools libxcb-cursor0 libevent-dev libegl1 libldap2-dev imagemagick libnss3 libxcomposite1 libxslt1.1 libldap-2.5-0 libsasl2-2 python3-dev ghostscript libgl1-mesa-glx unrar libxi6 && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# set the ImageMagik policy to allow PDF reads to extract images etc.
# https://github.com/janeczku/calibre-web/issues/827
COPY policy.xml /etc/ImageMagick-6/

# Calibre web
# renovate: datasource=github-releases depName=janeczku/calibre-web versioning=semver extractVersion=(?<version>\d+\.\d+\.\d+)
ARG CALIBRE_WEB_VERSION=0.6.24

RUN mkdir calibre-web && \
    cd calibre-web && \
    curl -L https://github.com/janeczku/calibre-web/archive/${CALIBRE_WEB_VERSION}.tar.gz | tar zxvf - --strip-component 1 && \
    pip3 install --no-cache-dir -U -r requirements.txt && \
    pip3 install --no-cache-dir -U -r optional-requirements.txt

# Install EPUB to KEPUB converter
# renovate: datasource=github-releases depName=geek1011/kepubify versioning=semver extractVersion=^v(?<version>.+)$
ARG KEPUBIFY_VERSION=4.0.4
RUN curl -L https://github.com/geek1011/kepubify/releases/download/v${KEPUBIFY_VERSION}/kepubify-linux-64bit -o /app/code/kepubify && chmod +x /app/code/kepubify

# Calibre. This will install into /app/code/calibre
# renovate: datasource=github-releases depName=kovidgoyal/calibre versioning=semver extractVersion=(?<version>\d+\.\d+\.\d+)
ARG CALIBRE_VERSION=7.26.0

RUN wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin install_dir=/app/code version=${CALIBRE_VERSION}
ENV LD_LIBRARY_PATH=/app/code/calibre/lib:$LD_LIBRARY_PATH

COPY alice-in-wonderland.epub start.sh getMailPasswordEncrypted.py /app/pkg/

CMD [ "/app/pkg/start.sh" ]

