import os
import sys

from pathlib import Path
from cryptography.fernet import Fernet

key = Path('/app/data/calibre-web/.key').read_text()

# see https://github.com/janeczku/calibre-web/blob/c2267b690293bc3932dc2588df1d907a7d365f28/cps/config_sql.py#L372
crypter = Fernet(key)
encryptedKey = crypter.encrypt(os.environ.get('CLOUDRON_MAIL_SMTP_PASSWORD').encode())

sys.stdout.write(encryptedKey.decode())
