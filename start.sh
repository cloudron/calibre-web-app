#!/bin/bash

set -eu

mkdir -p /app/data/calibre-web /run/calibre-web

readonly sqlite="gosu cloudron:cloudron sqlite3 /app/data/calibre-web/app.db"

setup() {
    set -eu


    echo "==> Ensuring calibre-web settings"
    $sqlite "UPDATE settings SET config_port=8083"
    $sqlite "UPDATE settings SET config_logfile='/dev/stdout'"

    # https://docs.python.org/2/library/logging.html#logging-levels
    $sqlite "UPDATE settings SET config_log_level=10"   # DEBUG: 10, INFO: 20

    $sqlite "UPDATE settings SET config_access_log=1"
    $sqlite "UPDATE settings SET config_access_logfile='/run/calibre-web/access.log'"
    $sqlite "UPDATE settings SET config_uploading=1"
    
    $sqlite "UPDATE settings SET mail_server='${CLOUDRON_MAIL_SMTP_SERVER}'"
    $sqlite "UPDATE settings SET mail_port='${CLOUDRON_MAIL_SMTP_PORT}'"
    $sqlite "UPDATE settings SET mail_use_ssl=0"
    $sqlite "UPDATE settings SET mail_login='${CLOUDRON_MAIL_SMTP_USERNAME}'"
    ENCRYPTED_PASSWORD=`python3 /app/pkg/getMailPasswordEncrypted.py`
    $sqlite "UPDATE settings SET mail_password_e='${ENCRYPTED_PASSWORD}'"
    $sqlite "UPDATE settings SET mail_from='${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Calibre-Web} <${CLOUDRON_MAIL_FROM}>'"
    
    $sqlite "UPDATE settings SET config_kepubifypath='/app/code/kepubify'"
    $sqlite "UPDATE settings SET config_converterpath='/app/code/calibre/bin/ebook-convert'" # the setting "config_calibre" is args to this converter
}

chown -R cloudron:cloudron /app/data /run/calibre-web

if [[ ! -f /app/data/calibre-web/app.db ]]; then
    # this creates the metadata.db
    echo "==> Creating empty library"
    gosu cloudron:cloudron /app/code/calibre/bin/calibredb add /app/pkg/alice-in-wonderland.epub --with-library /app/data/library/

    # create app.db schema. calibre reads all settings on startup, so we have to start it up twice
    echo "==> Init initial schema by setting the admin password"
    C_ALL=C.UTF-8 CALIBRE_DBPATH=/app/data/calibre-web gosu cloudron:cloudron python3 /app/code/calibre-web/cps.py -s admin:"Changeme?1234"

    echo "==> Set initial admin email to admin@cloudron.local"
    $sqlite "UPDATE user SET email='admin@cloudron.local' WHERE email='' and id=1"

    # do this only on first run, so that the user can move the library to a volume
    $sqlite "UPDATE settings SET config_calibre_dir='/app/data/library'"

    setup
else
    setup
fi

# https://github.com/janeczku/calibre-web/wiki/Command-Line-Interface
echo "=> Starting calibre-web"
cd calibre-web
LC_ALL=C.UTF-8 CALIBRE_DBPATH=/app/data/calibre-web exec gosu cloudron:cloudron python3 /app/code/calibre-web/cps.py
