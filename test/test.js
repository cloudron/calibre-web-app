#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function login(username='admin', password='Changeme?1234') {
        await browser.get(`https://${app.fqdn}/login`);
        await waitForElement(By.xpath('//input[@id="username"]'));
        await browser.findElement(By.xpath('//input[@id="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@id="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//button[@name="submit"]')).click();
        await waitForElement(By.xpath('//h2[text()="Books"]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/`);
        await waitForElement(By.xpath('//h2[text()="Books"]'));
        await browser.findElement(By.xpath('//a[@id="logout"]')).click();
        await waitForElement(By.xpath('//h2[text()="Login"]'));
    }

    async function changePassword(password='Foobar?1337') {
        await browser.get(`https://${app.fqdn}/`);
        await waitForElement(By.xpath('//h2[text()="Books"]'));
        await browser.findElement(By.xpath('//a[@id="top_user"]')).click();
        await browser.findElement(By.xpath('//input[@id="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//div[@id="user_submit"]')).click();
        await waitForElement(By.xpath('//div[@id="flash_success" and text()="Success! Profile Updated"]'));
    }

    async function createShelf(shelfName='CloudronTestShelf') {
        await browser.get(`https://${app.fqdn}/`);
        await waitForElement(By.xpath('//h2[text()="Books"]'));
        await browser.findElement(By.xpath('//li[@id="nav_createshelf"]')).click();
        await browser.findElement(By.xpath('//input[@id="title"]')).sendKeys(shelfName);
        await browser.findElement(By.xpath('//button[@id="submit"]')).click();
        await waitForElement(By.xpath(`//div[@id="flash_success" and text()="Shelf ${shelfName} created"]`));
        await browser.get(`https://${app.fqdn}/`);
        await waitForElement(By.xpath(`//li/a[contains(text(), "${shelfName}")]`));
    }

    async function findShelf(shelfName='CloudronTestShelf') {
        await browser.get(`https://${app.fqdn}/`);
        await waitForElement(By.xpath('//h2[text()="Books"]'));
        await waitForElement(By.xpath(`//li/a[contains(text(), "${shelfName}")]`));
    }

    async function uploadBook() {
        await browser.get(`https://${app.fqdn}/`);
        await waitForElement(By.xpath('//h2[text()="Books"]'));
        await browser.findElement(By.xpath('//input[@id="btn-upload"]')).sendKeys(`${__dirname}/cloudron_support.pdf`);
        await sleep(2000);
        await waitForElement(By.xpath('//input[@id="book_title" or @id="title"]'));
        await browser.findElement(By.xpath('//button[@id="submit"]')).click();
        await browser.get(`https://${app.fqdn}/`);
        await waitForElement(By.xpath('//h2[text()="Books"]'));
        await waitForElement(By.xpath('//div[@class="meta"]/a/p[text()="cloudron_support"]'));
    }

    async function findBook(bookName='cloudron_support') {
        await browser.get(`https://${app.fqdn}/`);
        await waitForElement(By.xpath(`//div[@class="meta"]/a/p[text()="${bookName}"]`));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('wait a bit', (done) => setTimeout(done, 15000)); // the start.sh script takes a bit to auto-configure. without this, you will see the setup UI
    it('can login', login);
    it('can change password', changePassword);
    it('can logout', logout);
    it('can login with new password', login.bind(null, 'admin', 'Foobar?1337'));
    it('can create shelf', createShelf);
    it('can find shelf', findShelf);
    it('can change password back', changePassword.bind(null, 'Changeme?1234'));
    it('can logout', logout);
    it('can login with defaults', login);
    it('can upload book', uploadBook);
    it('can find book', findBook);
    it('can logout', logout);
    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('can login', login);
    it('can find shelf', findShelf);
    it('can find book', findBook);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });
    it('can login', login);
    it('can find shelf', findShelf);
    it('can find book', findBook);
    it('can logout', logout);


    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('can login', login);
    it('can find shelf', findShelf);
    it('can find book', findBook);
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('can login', login);
    it('can find shelf', findShelf);
    it('can find book', findBook);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('can login', login);
    it('can find shelf', findShelf);
    it('can find book', findBook);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id calibreweb.janeczku.github --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('wait a bit', (done) => setTimeout(done, 15000)); // the start.sh script takes a bit to auto-configure. without this, you will see the setup UI
    it('can login', login.bind(null, 'admin', 'Changeme?1234'));
    it('can create shelf', createShelf);
    it('can upload book', uploadBook);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can login', login.bind(null, 'admin', 'Changeme?1234'));
    it('can find shelf', findShelf);
    it('can find book', findBook);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
